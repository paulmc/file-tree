# Changelog
## [Unreleased]
### Added
- A `link` keyword to `update_glob`, which allows one to indicate that two placeholders should co-vary.
### Changed
- `update_glob` will now always set the top-most level possible (i.e., instead of setting "input/subject" it will set "subject").
### Fixed
- In hierarchical placeholders using in sub-trees, "A/B/C" is now a child of "B/C" rather than "A/C" (both of which are children of "C").
## [v1.2.1]
### Fixed
- Rich is a dependency again as it is required for `Filetree.report`. Textual is still an optional dependency.
## [v1.2.0]
### Added
- Updated error message to suggest similar template key if template key is not found.
### Changed
- Rich and textual dependencies are now optional. Running the terminal user interface (`file-tree`) will require these to be installed.
- The terminal user interface now uses the newest version of textual.
## [v1.1.0]
### Added
- `glob_placeholders` flag in `convert` function to mark placeholders as wildcards
- In the CLI you can now define output targets based on their filename pattern in addition to using the template key.


[Unreleased]: https://git.fmrib.ox.ac.uk/ndcn0236/file-tree/-/compare/v1.2.1...master
[v1.2.0]: https://git.fmrib.ox.ac.uk/ndcn0236/file-tree/-/compare/v1.2.0...v1.2.1
[v1.2.0]: https://git.fmrib.ox.ac.uk/ndcn0236/file-tree/-/compare/v1.1.0...v1.2.0
[v1.1.0]: https://git.fmrib.ox.ac.uk/ndcn0236/file-tree/-/compare/v1.0.0...v1.1.0
